import numpy as np
# https://docs.scipy.org/doc/numpy/user/basics.subclassing.html#slightly-more-realistic-example-attribute-added-to-existing-array
class enp(np.ndarray):
    """Extension of Numpy ndarray class.

    This inherits from ndarray, and adds three new attributes, following the information given in https://docs.scipy.org/doc/numpy/user/basics.subclassing.html#slightly-more-realistic-example-attribute-added-to-existing-array
    This class allows automated creation of chunks from an existing np array.
    The size of the output array needs to be the chunk size + 2*overlap.

    :param chunk_size: size of the desired chunk
    :param overlap: how much the chunk is padded with data from neighbouring chunks
    :param location: location of top left corner of the chunk, situated in the original array

    :return: an empty array of the pnp class
    """

    # TODO: add a diagram to the docs
    def __new__(
        cls,
        input_array,
        location=None,
        overlap=None,
        chunk_size=None,
    ):
        obj = np.asarray(input_array).view(cls)
        obj.location = location
        obj.overlap = overlap
        obj.chunk_size = chunk_size
        return obj
    def __array_finalize__(self, obj):
        if obj is None:
            return
        self.location = getattr(obj, "location", None)
        self.overlap = getattr(obj, "overlap", None)
        self.chunk_size = getattr(obj, "chunk_size", None)

