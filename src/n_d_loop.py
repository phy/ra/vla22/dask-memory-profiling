import sys
sys.path.append(".")
from dask.distributed import performance_report, get_task_stream, Lock 
from dask.diagnostics import Profiler, ResourceProfiler, CacheProfiler, visualize
import helpers
import time
import configparser
from datetime import timedelta
import resource
from astropy.convolution import Gaussian2DKernel
from astropy.stats import sigma_clipped_stats
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
from photutils.detection import DAOStarFinder
from photutils.aperture import CircularAperture
import matplotlib.pyplot as plt
import numpy as np
import dask.array as da
from dask import delayed
import extnd_creator

class ProfileMe:
    def __init__(self, client, n_nodes, max_mem, n_workers, n_threads, worker_mem, platform, image_config, dask_version, workflow):
        self.client = client
        self.nodes = n_nodes
        self.max_mem = max_mem
        self.n_workers = n_workers
        self.n_threads = n_threads
        self.platform = platform
        self.worker_mem = int(worker_mem)
        self.image_config = image_config
        self.dask_version = dask_version
        self.workflow = workflow
        # create a config reader to allow us to provide values to the program
        config = configparser.ConfigParser()
        config.read('../config/config.ini')

        self.im_shape= [int(x) for x in config[image_config]['im_shape'].split(",")]
        self.ch_shape= []
        self.im_pix = 0
        self.ch_pix = 0
        self.n_dims=config[image_config].getint('n_dims')
        self.ch_shape_list = config[image_config]['ch_size_list'].split(";")
        self.program = ''
        self.filetype=''
        self.is_chunked='f'
        self.output_dir=config[platform]['output_dir']
        self.input_dir=config[platform]['input_dir']
        self.logger = helpers.setup_logger("standard_logger", self.output_dir + "all-tests.log")
        self.verifylog= helpers.setup_logger("verify_logger", self.output_dir + "all-verify.log")
        self.output_log = helpers.setup_logger("output_logger", self.output_dir + "all-output.log") 
        # list containing the programs available to test performance
        self.test_programs = config['DEFAULT']['programs'].split(",")
        self.files = config[image_config]['data_files'].split(",")
        self.verify = None
 
    def set_im_pix(self):
        temp_im_pix= 1
        for i in range(self.n_dims):
            temp_im_pix= temp_im_pix * self.im_shape[i]
        return temp_im_pix
        
    def set_ch_pix(self):
        temp_ch_pix= 1
        for i in range(self.n_dims):
            temp_ch_pix = temp_ch_pix * self.ch_shape[i]
        return temp_ch_pix
    
    def set_fileinfo(self, inputfile):
        if 'fits' in inputfile:
            self.filetype = 'fits'
        elif 'zarr' in inputfile:
            self.filetype = 'zarr'
        elif 'hdf5' in inputfile:
            self.filetype = 'hdf5'

        if 'chunk' in inputfile:
            self.is_chunked = 't'
        else:
            self.is_chunked = 'f'

    def name_file(self, program):
        filename =  self.output_dir + program + "d" + str(self.n_dims) + "i" + str(self.im_pix) + "c" + str(self.ch_pix) + "w"+ str(self.n_workers) + "t" + str(self.n_threads) + "m" + str(self.worker_mem) + self.filetype + self.is_chunked
        return filename

    def dask_process(self, image):
        self.logger.info("entered dask_process")
        print("entered dask process")
        client = self.client
        s_kernel = Gaussian2DKernel(x_stddev=1)                
        x = da.random.random((1000, 1000), chunks = (100,100))
        y = helpers.convolve_small(x, s_kernel)
        l_kernel = Gaussian2DKernel(x_stddev=5, y_stddev=5)
        self.logger.info(self.program)
        if self.program in ['np_array', 'dask_array']:
            if (self.n_dims == 3 and self.program == 'dask_array'):
                a = da.sum(image, axis=0)
            elif (self.n_dims ==3 and self.program == 'np_array'):
                a = np.sum(image, axis=0)
            mean, median, std = sigma_clipped_stats(a, sigma=3.0)
            daofind = DAOStarFinder(fwhm=3.0, threshold=5.*std)
            sources = daofind(a - median)
            for col in sources.colnames:
                if col not in ('id', 'npix'):
                    sources[col].info.format = '%.2f'
            sources.write(self.name_file(self.program) +'sources.csv', format='ascii.csv')
            positions = np.transpose((sources['xcentroid'], sources['ycentroid']))
            apertures = CircularAperture(positions, r=4.0)
            norm = ImageNormalize(stretch=SqrtStretch())
            plt.imshow(a, cmap='Greys', origin='lower', norm=norm, interpolation='nearest')
            apertures.plot(color='blue', lw=1.5, alpha=0.5)
            plt.savefig(self.name_file(self.program) + '.png')
            a = helpers.add(image, 1)
            b = helpers.mult(a, 2)
            c = helpers.exp(b, 3)                
            d = helpers.convolve_small(c, s_kernel)
            if self.program == 'np_array':
                e = np.ones(image.shape, dtype=image.dtype)
            else:
                e = da.ones(image.shape, chunks=self.ch_shape, dtype=image.dtype)
            f = helpers.subtract(d, e)
            g = helpers.convolve_large(f, l_kernel) 
            if self.program == 'dask_array':
                g.compute()
            return g

        elif self.program in ['dask_fut', 'dask_fut_scatter']:
            self.logger.info("etnered dask_fut branch")
            print("DaskFutures branch")
            if self.program == 'dask_fut_scatter':
                image = client.scatter(image)
                self.logger.info("image scattered")
            a = client.submit(da.sum, image, axis=0)
            c = client.gather(a)
            std = client.submit(sigma_clipped_stats, c, sigma=3.0)
            stdd = std.result()
            daofind = DAOStarFinder(fwhm=3.0, threshold=5.*stdd[2])
            sources = client.submit(daofind, c - stdd[1])
            result = sources.result()
            for col in result.colnames:
                if col not in ('id', 'npix'):
                    result[col].info.format = '%.2f'
            result.write(self.name_file(self.program) +'sources.csv', format='ascii.csv')
            positions = np.transpose((result['xcentroid'], result['ycentroid']))
            apertures = CircularAperture(positions, r=4.0)
            norm = ImageNormalize(stretch=SqrtStretch())
            plt.imshow(c, cmap='Greys', origin='lower', norm=norm, interpolation='nearest')
            apertures.plot(color='blue', lw=1.5, alpha=0.5)
            plt.savefig(self.name_file(self.program) + '.png')
            a = client.submit(helpers.add, image, 1)
            b = client.submit(helpers.mult, a, 2)
            c = client.submit(helpers.exp, b, 3)
            d = client.submit(helpers.convolve_small, b, s_kernel)
            if self.program == 'dask_fut':
                e = da.ones(image.shape, dtype=np.uint8)
            else:
                he = client.gather(d)
                e = da.ones(he.shape, dtype=np.uint8)
            f = client.submit(helpers.subtract, d, e)
            g = client.submit(helpers.convolve_large, f, l_kernel)
            if self.program =='dask_fut_scatter':
                result = client.gather(g)
                self.logger.debug(type(result))
                return result
            else:
                return g.result()

        elif self.program in ['np_chunk','np_ext','np_chunk_delayed', 'np_ext_delayed', 'np_ext_fut', 'np_ext_fut_scatter']:
            self.logger.info("entered np branch, %s im shape, %s ch_shape", image.shape, self.ch_shape)
            if self.program in ["np_chunk", "np_chunk_delayed"]:
                chunk_array = extnd_creator.create_array(image, self.ch_shape, self.logger, kind="np")
            else:
                chunk_array = extnd_creator.create_array(image, self.ch_shape, self.logger)
            self.logger.info("Created %s chunks of size %s from image %s", len(chunk_array), self.ch_shape, image.shape)
            if self.program in ['np_ext_fut_scatter']:
                chunk_array = client.scatter(chunk_array)

            if self.program in ['np_ext','np_chunk']:
                j = 1
                for i in chunk_array:
                    j = j + 1
                    a = np.sum(i, axis=0)
                    mean, median, std = sigma_clipped_stats(a, sigma=3.0)
                    if self.program == 'np_ext':
                        mean = float(mean)
                        median = float(median)
                        std = float(std)
                    daofind = DAOStarFinder(fwhm=3.0, threshold=5.*std)
                    sources = daofind(a - median)
                    for col in sources.colnames:
                        if col not in ('id', 'npix'):
                            sources[col].info.format = '%.2f'
                    sources.write(self.name_file(self.program) + str(j) + 'sources.csv', format='ascii.csv')
                    #positions = np.transpose((sources['xcentroid'], sources['ycentroid']))
                    #iapertures = CircularAperture(positions, r=4.0)
                    #inorm = ImageNormalize(stretch=SqrtStretch())
                    #plt.imshow(a, cmap='Greys', origin='lower', norm=norm, interpolation='nearest')
                    #apertures.plot(color='blue', lw=1.5, alpha=0.5)
                    #plt.savefig(self.name_file(self.program) + str(j) + 'sourcedet.png')
                    i = helpers.add(i, 1)
                    i = helpers.mult(i, 2)
                    i = helpers.exp(i, 3)
                    i = helpers.convolve_small(i, s_kernel)
                    k = np.ones(i.shape, dtype=np.uint8)
                    i = helpers.subtract(i, k)
                    i = helpers.convolve_large(i, l_kernel)
                return chunk_array

            elif self.program in ['np_chunk_delayed', 'np_ext_delayed']:
                print(self.program, type(chunk_array))
                lock = Lock()
                j = 0
                for i in chunk_array:
                    with lock:
                        j = j + 1
                    a = delayed(np.sum)(i, axis=0)
                    mmstd = delayed(sigma_clipped_stats)(a, sigma=3.0)
                    mmstd = mmstd.compute()
                    mean = float(mmstd[0])
                    median = float(mmstd[1])
                    std = float(mmstd[2])
                    daofind = delayed(DAOStarFinder)(fwhm=3.0, threshold=5.*std)
                    sources = delayed(daofind)(a - median)
                    sources = sources.compute()
                    for col in sources.colnames:
                        if col not in ('id', 'npix'):
                            sources[col].info.format = '%.2f'
                    sources.write(self.name_file(self.program) + str(j) + 'sources.csv', format='ascii.csv')
                    positions = np.transpose((sources['xcentroid'], sources['ycentroid'])) 
                    apertures = CircularAperture(positions, r=4.0) 
                    norm = ImageNormalize(stretch=SqrtStretch()) 
                    a = a.compute()
                    plt.imshow(a, cmap='Greys', origin='lower', norm=norm, interpolation='nearest') 
                    apertures.plot(color='blue', lw=1.5, alpha=0.5) 
                    plt.savefig(self.name_file(self.program) + str(j) + 'sourcedet.png')


                    #a = delayed(helpers.add)(i, 1)
                    #b = delayed(helpers.mult)(a, 2)
                    #c = delayed(helpers.exp)(b, 3)
                    #d = delayed(helpers.convolve_small)(c, s_kernel)
                    #e = delayed(np.ones)(i.shape, dtype=np.uint8)
                    #f = delayed(helpers.subtract)(d, e)
                    #g = delayed(helpers.convolve_large)(f, l_kernel)
                    #g.compute()
                return chunk_array
            
            elif self.program in ['np_ext_fut', 'np_ext_fut_scatter']:
                output = []
                lock = Lock()
                k = 0
                for i in chunk_array:
                    with lock:
                        k = k + 1
                    a = client.submit(da.sum, i, axis=0)
                    if self.program == 'np_ext_fut_scatter':
                        a = client.gather(a)
                    std = client.submit(sigma_clipped_stats, a, sigma=3.0)
                    stdd = std.result()
                    daofind = DAOStarFinder(fwhm=3.0, threshold=5.*stdd[2])
                    a = a.result()
                    sources = client.submit(daofind, a - stdd[1])
                    result = sources.result()
                    for col in result.colnames:
                        if col not in ('id', 'npix'):
                            result[col].info.format = '%.2f'
                    result.write(self.name_file(self.program) + str(k) +'sources.csv', format='ascii.csv')
                    positions = np.transpose((result['xcentroid'], result['ycentroid']))
                    apertures = CircularAperture(positions, r=4.0)
                    norm = ImageNormalize(stretch=SqrtStretch())
                    plt.imshow(a, cmap='Greys', origin='lower', norm=norm, interpolation='nearest')
                    apertures.plot(color='blue', lw=1.5, alpha=0.5)
                    plt.savefig(self.name_file(self.program) + str(k) + '.png')
                    a = client.submit(helpers.add, i, 1)
                    b = client.submit(helpers.mult, a, 2)
                    c = client.submit(helpers.exp, b, 3)
                    d = client.submit(helpers.convolve_small, b, s_kernel)
                    e = np.ones(self.ch_shape, dtype=np.uint8)
                    f = client.submit(helpers.subtract, d, e)
                    g = client.submit(helpers.convolve_large, f, l_kernel)
                    h = g.result()
                    output.append(h)
                return output
# An empty function that can be used to test loop logic without invoking real functions.
    def passthrough():
        return
#utility to iterate over the different dask configurations
    
    def load_file(self, i):
        image = None
        if self.program in ['np_array', 'np_chunk', 'np_chunk_delayed', 'np_ext', 'np_ext_fut', 'np_ext_delay', 'np_ext_fut_scatter']:
            print(self.im_pix, ((self.worker_mem * 1e10)/5))
            if self.program == 'np_array' and self.im_pix > ((self.worker_mem * 1e7)/5):
                raise ValueError
            #todo provide error message
            else:
                image = helpers.load_file(self.filetype, i, self.im_shape, 'np')
        else:
            image = helpers.load_file(self.filetype, i, self.im_shape, 'dask', chunk_shape=self.ch_shape)
            if self.filetype == 'fits':
                image = da.from_array(image, chunks=(self.ch_shape))
        return image
    
    def launch_me(self):
        self.logger.info("entered launch_me loop")
        #iterate over the different types of input files
        for i in self.files:
            self.set_fileinfo(i)
            i = self.input_dir + i
            self.logger.info("program = %s, nodes=%s, image = %s, threads/worker=%s, workers=%s, mem=%s, file=%s", self.program, self.nodes, self.ch_pix, self.n_threads, self.n_workers, self.worker_mem, i)
            start_time = time.monotonic()
            filename = self.name_file(self.program)
            with performance_report(filename=filename + "perf_report.html"), Profiler() as prof, ResourceProfiler(dt=0.05) as rprof, CacheProfiler() as cprof:
                self.logger.info("filetype = %s", self.filetype)
                try: 
                    image = self.load_file(i)
                except ValueError:
                    print("numpy array too large %s", self.im_pix)
                    self.logger.info("numpy array too large: %s", self.im_pix)
                load_finished = time.monotonic()
                with get_task_stream(
                    plot="save", filename=(filename + "task_stream.html")) as ts:
                    g = None
                    try:
                        g = self.dask_process(image)
                    except Exception as e:
                        print(e)
                        self.logger.info(str(e))
                        continue
                    if self.program in ['np_array', 'dask_array', 'dask_fut', 'dask_fut_scatter']:
                        print("verify")
                        self.verify = np.array(g[:5, :5])
                    else:
                        print(self.test_programs)
                        self.verify = np.array(g[0][:5, :5])
                self.verifylog.info(self.verify)
                self.logger.info(filename)
                calc_done = time.monotonic()
                try:
                    if self.program == 'np_array':
                        helpers.save_file(filename, self.filetype, g)
                    elif self.program in ['np_chunk', 'np_chunk_delayed','np_ext', 'np_ext_fut', 'np_ext_delayed', 'np_ext_fut_scatter']:
                        output_array = np.array(g)
                        helpers.save_file(filename, self.filetype, output_array, is_np=True)
                    else:
                        helpers.save_file(filename, self.filetype, g, chunks=self.ch_shape)
                except Exception as e:
                    self.logger.info(str(e))
                    self.logger.info(self.program)
                    self.logger.error(filename)
                    continue
                mem_used = helpers.format_bytes(
                resource.getpagesize() * resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
            end_time = time.monotonic()
            self.output_log.info(
                ", %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s",
                self.platform,
                self.dask_version,
                self.workflow,
                self.nodes,
                self.n_workers,
                self.n_threads,
                self.worker_mem,
                self.n_dims,
                self.im_pix,
                self.ch_pix,
                self.filetype,
                self.is_chunked,
                timedelta(seconds = end_time - start_time),
                timedelta(seconds = load_finished - start_time),
                timedelta(seconds = calc_done - load_finished),
                timedelta(seconds = end_time - calc_done),
                mem_used,
                )
            visualize([prof, rprof, cprof], filename=(filename + "profile.html"), save=True)
    def image_param_iter(self, function):
    #loop over the list of programs, image size, and chunk size (where applicable)
        for i in self.test_programs:
            self.im_pix = self.set_im_pix()
            self.program = i
            self.output_log=helpers.setup_logger("output_logger", self.output_dir + i + ".csv")
            for i in self.ch_shape_list:
                i = [int(x) for x in i.split(",")]
                self.ch_shape = i
                self.ch_pix = self.set_ch_pix()
                self.logger.info("%s, %s", self.ch_shape, self.ch_pix)
                # This is trying to make sure that a chunk isn't >10% of total node memory
                if self.ch_pix > self.worker_mem * 1000000:
                    self.logger.info("Chunk too large to fit in RAM")
                else:
                    self.launch_me()


