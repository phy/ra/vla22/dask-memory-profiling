import numpy as np

# https://docs.scipy.org/doc/numpy/user/basics.subclassing.html#slightly-more-realistic-example-attribute-added-to-existing-array
class pnp(np.ndarray):
    """Extension of Numpy ndarray class.

    This inherits from ndarray, and adds three new attributes, following the information given in https://docs.scipy.org/doc/numpy/user/basics.subclassing.html#slightly-more-realistic-example-attribute-added-to-existing-array
    This class allows automated creation of chunks from an existing np array.

    :param chunk_size: size of the desired chunk
    :param overlap: how much the chunk is padded with data from neighbouring chunks
    :param location: location of top left corner of the chunk, situated in the original array

    :return: an empty array of the pnp class
    """

    # TODO: add a diagram to the docs
    def __new__(
        cls,
        shape,
        dtype=float,
        buffer=None,
        offset=0,
        strides=None,
        order=None,
        location=None,
        overlap=None,
        chunk_size=None,
    ):
        obj = super(pnp, cls).__new__(cls, shape, dtype, buffer, offset, strides, order)
        obj.location = location
        obj.overlap = overlap
        obj.chunk_size = chunk_size
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        self.location = getattr(obj, "location", None)
        self.overlap = getattr(obj, "overlap", None)
        self.chunk_size = getattr(obj, "chunk_size", None)

    def fill_me(self, input_array, fill_value=1):
        # how to deal with dimensionality? (3D, 4D)
        """Fills the pnp array with data from the input_array.

        :param input_array: array from which the data is copied
        :param fill_value: determines the value with which chunks at the border of the input array are padded.
        """
        loc = self.location
        overlap = self.overlap
        chunk_size = self.chunk_size
        # print("chunk shape: %s", chunk_size.shape)
        if (loc[0] == 0) & (loc[1] == 0):
            print("top-left")
            print(
                type(overlap),
                type(input_array),
                type(chunk_size),
                type(chunk_size[0]),
                type(loc),
                type(loc[0]),
            )
            self[overlap:, overlap:] = np.ndarray.copy(
                input_array[
                    loc[0] : (loc[0] + chunk_size[0] + overlap),
                    loc[1] : (loc[1] + chunk_size[1] + overlap),
                ]
            )
            print(type(fill_value))
            self[0, :].fill(fill_value)
            self[:, 0].fill(fill_value)
        elif (loc[0] == 0) & (loc[1] + chunk_size[0] + overlap >= input_array.shape[1]):
            print("top-right")
            self[overlap:, : self.shape[1] - overlap] = np.ndarray.copy(
                input_array[
                    loc[0] : (loc[0] + chunk_size[0] + overlap),
                    loc[1] - overlap : input_array.shape[1],
                ]
            )
            self[0, :].fill(fill_value)
            self[:, self.shape[1] - 1].fill(fill_value)
        elif loc[0] == 0:
            print("top")
            self[overlap:, :] = np.ndarray.copy(
                input_array[
                    loc[0] : loc[0] + chunk_size[0] + overlap,
                    loc[1] - overlap : loc[1] + chunk_size[1] + overlap,
                ]
            )
            self[0, :].fill(fill_value)
        elif ((loc[0] + chunk_size[0] + overlap) >= input_array.shape[0]) & (
            loc[1] == 0
        ):
            print("bottom-left")
            self[: self.shape[0], overlap:] = np.ndarray.copy(
                input_array[
                    loc[0] - overlap : input_array.shape[0],
                    loc[1] : (loc[1] + chunk_size[1] + overlap),
                ]
            )
            self[self.shape[0] - 1, :].fill(fill_value)
            self[:, 0].fill(fill_value)
        elif (loc[0] + chunk_size[0] + overlap >= input_array.shape[0]) & (
            loc[1] + chunk_size[0] + overlap >= input_array.shape[1]
        ):
            print("bottom-right")
            self[: self.shape[0] - overlap, : self.shape[1] - overlap] = np.ndarray.copy(
                input_array[
                    loc[0] - overlap : loc[0] + chunk_size[0] + overlap,
                    loc[1] - overlap : input_array.shape[1],
                ]
            )
            self[self.shape[0] - 1, :].fill(fill_value)
            self[:, self.shape[1] - 1].fill(fill_value)
        elif (loc[0] + chunk_size[0] + overlap) >= input_array.shape[0]:
            print("bottom")
            self[: self.shape[0] - overlap, :] = np.ndarray.copy(
                input_array[
                    loc[0] - overlap : input_array.shape[0],
                    (loc[1] - overlap) : (loc[1] + chunk_size[1] + overlap),
                ]
            )
            self[self.shape[0] - 1, :].fill(fill_value)
        elif loc[1] == 0:
            print("left")
            self[:, overlap:] = np.ndarray.copy(
                input_array[
                    loc[0] - overlap : loc[0] + chunk_size[0] + overlap,
                    loc[1] : loc[1] + chunk_size[1] + overlap,
                ]
            )
            self[:, 0].fill(fill_value)
        elif (loc[1] + chunk_size[0] + overlap) >= input_array.shape[1]:
            print("right")
            self[:, : self.shape[1] - overlap] = np.ndarray.copy(
                input_array[
                    loc[0] - overlap : loc[0] + chunk_size[0] + overlap,
                    loc[1] - overlap : input_array.shape[1],
                ]
            )
            self[:, self.shape[1] - 1].fill(fill_value)

        else:
            print("middle")
            self[:, :] = np.ndarray.copy(
                input_array[
                    loc[0] - overlap : (loc[0] + chunk_size[0] + overlap),
                    (loc[1] - overlap) : (loc[1] + chunk_size[1] + overlap),
                ]
            )
