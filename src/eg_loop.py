from dask.distributed import performance_report, get_task_stream
from dask.diagnostics import Profiler, ResourceProfiler, CacheProfiler, visualize
import helpers
import time
import configparser
from datetime import timedelta
import resource
from astropy.convolution import Gaussian2DKernel
import numpy as np
import dask.array as da
from dask import delayed
import extnd as ext_np

class ProfileMe:
    """
    Class to provide us with a dict to store useful parameters in, to be used in running workloads to be profiled.
    :param program: This says which program is currently being run
    :param workers: Number of Dask workers available to the client.
    :param threads: Number of threads per worker.
    :param worker_mem: Size of RAM in GB available to each Dask worker.
    :param min_mem: Smallest size of worker memory available. Used to reset sizes for different client configs.
    :param max_mem: size of RAM available to each worker. This size should be smaller than the total RAM available on a node.
    :param image_size: int that defines the size of the image being processed.
    :param chunk_size: int that defines the size of chunks used to break up the image into manageable parts.
    :param min_chunk_size: int defining the smallest chunk size to be used. Used to reset after certain config loops.
    :param chunk_limit: int defining maximum chunk size. This should usally be set to no more than 1/2 the maximum dimension of the image size.
    :param image_limit: int representing the largest image size you wish to process.
    :params n_dims: number of dimension to be used to define the array. So if chunk_size is 256, and n_dims is 2, then the size of chunks used will be (256,256)
    :param log: a reference to a log file that can be used by each program to record logging output.
    :param output_log: records the performance parameters logged during processing.
    """
    def __init__(self, client, worker_mem, threads, workers, n_nodes):
        # create a config reader to allow us to provide values to the program
        config = configparser.ConfigParser()
        config.read('../config/config.ini')
        self.params = dict(
            program="",
            n_nodes=n_nodes,
            workers=workers,
            threads=threads,
            worker_mem=worker_mem,
            max_mem=config['system-params'].getint('max_mem'),
            image_size=config['program-params'].getint('starting_image_size'),
            chunk_size=config['program-params'].getint('min_chunk_size'),
            min_chunk_size=config['program-params'].getint('min_chunk_size'),
            chunk_limit=0,
            image_limit=config['program-params'].getint('image_limit'),
            n_dims=config['program-params'].getint('n_dim'),
            filetype='',
            is_chunked='f',
            client=client,
            output_dir=config['system-params']['output_dir'],
        )

        self.logger = helpers.setup_logger("standard_logger", self.params['output_dir'] + "all-tests.log")
        self.verifylog= helpers.setup_logger("verify_logger", self.params['output_dir'] + "all-verify.log")
        self.verify = None
        self.output_log = helpers.setup_logger("output_logger", self.params['output_dir'] + "all-output.log") 
        # list containing the programs available to test performance
        self.test_programs = config['program-params']['programs'].split(",")

        # list of locations of files for loading testss
        self.files = config['program-params']['data'].split(",")
 
    # Two short utilities to turn a single value for image and chunk size into an n-dimensional array with that value for each dimension. Not currently used
    def set_im_params(self):
        temp_im_size = []
        for i in range(self.params['n_dims']):
            temp_im_size.append(self.params['image_size'])
        a = tuple(temp_im_size)
        return a

    def set_chunk_params(self):
        temp_chunk_size =[]
        for i in range(self.params['n_dims']):
            temp_chunk_size.append(self.params['chunk_size'])
        a = tuple(temp_chunk_size)
        return a

    def name_file(self, program):
        filename =  self.params['output_dir'] + program + "d" + str(self.params['n_dims']) + "i" + str(self.params['image_size']) + "c" + str(self.params['chunk_size']) + "t"+ str(self.params['threads']) + "w" + str(self.params['workers']) + "m" + str(self.params['worker_mem']) + self.params['filetype'] + self.params['is_chunked']
        return filename

    def dask_process(self, image):
        self.logger.info("entered dask_process")
        client = self.params['client']
        s_kernel = Gaussian2DKernel(x_stddev=1)                
        l_kernel = Gaussian2DKernel(x_stddev=5, y_stddev=5)
        self.logger.info(self.params['program'])
        if self.params['program'] in ['np_array', 'dask_array']:
            a = helpers.add(image, 1)
            b = helpers.mult(a, 2)
            c = helpers.exp(b, 3)                
            d = helpers.convolve_small(c, s_kernel)
            if self.params['program'] == 'np_array':
                e = np.ones(image.shape)
            else:
                e = da.ones(image.shape, chunks=self.set_chunk_params())
            f = helpers.subtract(d, e)
            g = helpers.convolve_large(f, l_kernel) 
            g.compute()
            return g

        elif self.params['program'] in ['dask_fut', 'dask_fut_scatter']:
            self.logger.info(type(image))
            if self.params['program'] == 'dask_fut_scatter':
                image = client.scatter(image)
            a = client.submit(helpers.add, image, 1)
            b = client.submit(helpers.mult, a, 2)
            c = client.submit(helpers.exp, b, 3)
            d = client.submit(helpers.convolve_small, b, s_kernel)
            e = da.ones(self.set_im_params(), chunks=self.set_chunk_params())
            f = client.submit(helpers.subtract, d, e)
            g = client.submit(helpers.convolve_large, f, l_kernel)
            if self.params['program'] =='dask_fut_scatter':
                result = client.gather(g)
                self.logger.info(type(result))
                return result
            else:
                return g.result()

        elif self.params['program'] in ['np_chunk','np_ext','np_chunk_delayed', 'np_ext_delay', 'np_ext_fut', 'np_ext_fut_scatter']:
            i = 0
            j = 0
            chunk_array= []
            im_size = self.set_im_params()
            ch_size = self.set_chunk_params()
            while i < im_size[0]:
                while j < im_size[1]:
                    if self.params['program'] == 'np_chunk' or 'np_chunk_delayed':
                        chunk = image[i : (i + ch_size[0]), j : (j + ch_size[1])]
                    else:
                        chunk = ext_np.pnp(ch_size, dtype=float, location=(i,j), overlap=0, chunk_size=ch_size)
                        chunk.fill_me(image)
                    chunk_array.append(chunk)
                    j = j + ch_size[1]
                j = 0
                i = i + ch_size[0]
            self.logger.info("Created %s chunks of size %s", len(chunk_array), ch_size)
            if self.params['program'] in ['np_ext_fut_scatter']:
                chunk_array = client.scatter(chunk_array)

            if self.params['program'] in ['np_ext','np_chunk']:
                for i in chunk_array:
                    a = helpers.add(i, 1)
                    b = helpers.mult(a, 2)
                    c = helpers.exp(b, 3)
                    d = helpers.convolve_small(c, s_kernel)
                    e = np.ones(ch_size)
                    f = helpers.subtract(d, e)
                    g = helpers.convolve_large(f, l_kernel)
                    g.compute()
                return chunk_array

            elif self.params['program'] in ['np_chunk_delayed', 'np_ext_delay']:
                for i in chunk_array:
                    a = delayed(helpers.add)(i, 1)
                    b = delayed(helpers.mult)(a, 2)
                    c = delayed(helpers.exp)(b, 3)
                    d = delayed(helpers.convolve_small)(c, s_kernel)
                    e = delayed(np.ones)(ch_size)
                    f = delayed(helpers.subtract)(d, e)
                    g = delayed(helpers.convolve_large)(f, l_kernel)
                    g.compute()
                return chunk_array
            
            elif self.params['program'] in ['np_ext_fut', 'np_ext_fut_scatter']:
                output = []
                for i in chunk_array:
                    a = client.submit(helpers.add, i, 1)
                    b = client.submit(helpers.mult, a, 2)
                    c = client.submit(helpers.exp, b, 3)
                    d = client.submit(helpers.convolve_small, b, s_kernel)
                    e = np.ones(ch_size)
                    f = client.submit(helpers.subtract, d, e)
                    g = client.submit(helpers.convolve_large, f, l_kernel)
                    h = g.result()
                    output.append(h)
                return output
# An empty function that can be used to test loop logic without invoking real functions.
    def passthrough():
        return
#utility to iterate over the different dask configurations
    def launch_me(self):
        self.logger.info("entered launch_me loop")
        im_size = self.set_im_params()
        ch_size = self.set_chunk_params()
        #iterate over the different types of input files
        for i in self.files:
            #set params based on filetypes and whether chunks were used
            if 'fits' in i:
                self.params['filetype'] = 'fits'
            elif 'zarr' in i:
                self.params['filetype'] = 'zarr'
            elif 'hdf5' in i:
                self.params['filetype'] = 'hdf5'

            if 'chunk' in i:
                self.params['is_chunked'] = 't'
            else:
                self.params['is_chunked'] = 'f'
            self.logger.info("program = %s,image = %s, threads/worker=%s, workers=%s, mem=%s, file=%s", self.params['program'], self.params['image_size'], self.params['threads'], self.params['workers'], self.params['worker_mem'], i)
            start_time = time.monotonic()
            filename = self.name_file(self.params['program'])
            with performance_report(filename=filename + "perf_report.html"), Profiler() as prof, ResourceProfiler(dt=0.05) as rprof, CacheProfiler() as cprof:
                if self.params['program'] in ['np_array', 'np_chunk', 'np_chunk_delayed', 'np_ext', 'np_ext_fut', 'np_ext_delay', 'np_ext_fut_scatter']:
                    image = helpers.load_file(self.params['filetype'], i, im_size, 'np')
                else:
                    image = helpers.load_file(self.params['filetype'], i, im_size, 'dask', chunk_shape=ch_size)
                    if self.params['filetype'] == 'fits':
                        image = da.from_array(image, chunks=(ch_size))
                load_finished = time.monotonic()
                
                with get_task_stream(
                    plot="save", filename=(filename + "task_stream.html")) as ts:
                    try:
                        g = self.dask_process(image)
                    except Exception as e:
                        self.logger.info(str(e))
                        continue
                    if self.params['program'] in ['np_array', 'dask_array', 'dask_fut', 'dask_fut_scatter']:
                        self.verify = np.array(g[:5, :5])
                    else:
                        self.verify = np.array(g[0][:5, :5])
                self.verifylog.info(self.verify)
                self.logger.info(filename)
                calc_done = time.monotonic()
                try:
                    if self.params['program'] == 'np_array':
                        helpers.save_file(filename, self.params['filetype'], g)
                    elif self.params['program'] in ['np_chunk', 'np_chunk_delayed','np_ext', 'np_ext_fut', 'np_ext_delay', 'np_ext_fut_scatter']:
                        output_array = np.array(g)
                        helpers.save_file(filename, self.params['filetype'], output_array, is_np=True)
                    else:
                        helpers.save_file(filename, self.params['filetype'], g, chunks=ch_size)
                except Exception as e:
                    self.logger.info(str(e))
                    self.logger.info(self.params['program'])
                    continue
                mem_used = helpers.format_bytes(
                resource.getpagesize() * resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
            end_time = time.monotonic()
            self.output_log.info(
                ", %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s",
                self.params['n_nodes'],
                self.params['workers'],
                self.params['threads'],
                self.params['worker_mem'],
                self.params['n_dims'],
                self.params['image_size'],
                self.params['chunk_size'],
                self.params['filetype'],
                self.params['is_chunked'],
                timedelta(seconds = end_time - start_time),
                timedelta(seconds = load_finished - start_time),
                timedelta(seconds = calc_done - load_finished),
                timedelta(seconds = end_time - calc_done),
                mem_used,
                )
            visualize([prof, rprof, cprof], filename=(filename + "profile.html"), save=True)

    def image_param_iter(self, function):
    #loop over the list of programs, image size, and chunk size (where applicable)
        for i in self.test_programs:
            self.params['program'] = i
            self.output_log=helpers.setup_logger("output_logger", self.params['output_dir'] + i + ".csv")
            self.params['image_size'] = 4 * self.params['min_chunk_size']
            self.params['chunk_size'] = self.params['min_chunk_size']
            while self.params['image_size'] <= self.params['image_limit']:
                self.chunk_limit = self.params['image_size'] / 2
                self.logger.info("entered image size iteration loops")
                if i == 'np_array':
                    if self.params['image_size'] * 1.5 >= self.params['worker_mem'] * 2048:
                        self.logger.info("image size %s greater than worker memory %s", self.params['image_size'], self.params['worker_mem'])
                        self.params['image_size'] == self.params['image_limit']
                    else:
                        self.params['client']
                        self.launch_me()
                # set limit for chunk sizes based on image size
                else:
                    self.params['chunk_limit'] = (self.params['image_size'] // 2) + 1
                    while self.params['chunk_size'] <= self.chunk_limit:
                        self.params['client']
                        self.launch_me()
                        self.params['chunk_size'] = self.params['chunk_size'] * 2
                self.params['image_size'] = self.params['image_size'] * 2
                self.params['chunk_size'] = self.params['min_chunk_size']


