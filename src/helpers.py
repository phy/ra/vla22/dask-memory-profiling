import sys
sys.path.append(".")
import logging
import h5py
import zarr
from astropy.io import fits
from astropy.convolution import convolve, Gaussian2DKernel
import dask.array as da
import numpy as np
# from mpi4py import MPI
#todo make fully general for loading 1-4D of data from each file.

def arr_size_check(source_arr, arr_size):
    if len(source_arr.shape) < len(arr_size):
        raise ValueException("You've asked for an array with more dimensionsthan the array stored")
        for i in source_arr.shape:
            j = d.shape[i] - arr_size[i]
            if j < 0:
                raise ValueException("Desired array size is larger than the stored array in at least position %s", i)
    else:
        return True

def load_file(filetype, filepath, array_size, prog_type, chunk_shape=None):
    """Takes a filetype and path and returns an array object suitable for use by dask (& possibly numpy?)"""
    if filetype == "hdf5":
        f = h5py.File(filepath, "r")
        # code to find the data path
        d = f["array"]
        arr_check = arr_size_check(d, array_size)
        if arr_check != True:
            raise ValueException("Desired array incompatible with source data")
        print(d.shape)
        if prog_type == 'dask':
            output = da.from_array(d[:array_size[0], :array_size[1]], chunks=chunk_shape)
            return output
            
        else:
            output = np.array(d[:array_size[0], :array_size[1]])
            return output
        return d
        f.close()
    elif filetype == "zarr":
        f = zarr.open(filepath, mode="r")
        arr_check = arr_size_check(f, array_size)
        if arr_check != True:
            raise ValueException("Desired array incompatible with source data")
        if prog_type == 'np':
            a = np.array(f[:array_size[0], :array_size[1]])
            return a
        elif prog_type == 'dask':
            a = da.from_array(f[:array_size[0], :array_size[1]], chunks=chunk_shape)
            return a

    elif filetype == "fits":
        f = fits.open(filepath)
        arr_check = arr_size_check(f[0].data, array_size)
        if arr_check != True:
            raise ValueException("Desired array incompatible with source data")
        if 'SKAMid_B5_8h_v3.fits' in filepath:
            data = f[0].data
            d = data[0, 0, :array_size[0], :array_size[1]]

        else:
            d = f[0].data
        return d
    else:
        raise ValueException(
            "File type not supported. Supported filetypes are hdf5, zarr, and fits."
        )


def save_file(path, filetype, array, driver=None, chunks=None, processes=None, is_np=False):

    filename = path + '.' + filetype 
    if filetype == "hdf5":
        a = da.ones(10)
        b = np.ones(2)
        if type(a) == type(array):
            da.to_hdf5(filename, {'array': array})
        else:
            arr = da.from_array(array)
            da.to_hdf5(filename, {'array': arr})

    elif filetype == "zarr":
        if is_np == True:
            zarr.convenience.save(filename, array)
        else:
            array.to_zarr(filename)

    elif filetype == "fits":
        array = np.array(array)
        hdu = fits.PrimaryHDU(array)
        hdul = fits.HDUList([hdu])
        hdul.writeto(filename, overwrite=True)
    else:
        raise Exception(
            "File type not supported. Supported filetypes are hdf5, zarr, and fits."
        )


def setup_logger(name, log_file, level=logging.INFO):
    """allows multiple loggers to be set up"""
    formatter = logging.Formatter(
        "%(asctime)s p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
    )
    alt_formatter = logging.Formatter("%(asctime)s %(message)s", datefmt="%Y-%m-%d %H:%M:%S")

    handler = logging.FileHandler(log_file)
    if name == "standard_logger":
        handler.setFormatter(formatter)
    else:
        handler.setFormatter(alt_formatter)

    this_log = logging.getLogger(name)
    this_log.setLevel(level)
    this_log.addHandler(handler)

    return this_log


def format_bytes(size):
    # A short routine taken from https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb to display memory usage usefully
    power = 2 ** 10  # i.e. 1024
    n = 0
    output_labels = {0: "", 1: "K", 2: "M", 3: "G", 4: "T", 5: "P"}
    while size > power:
        size /= power
        n += 1
    return (size, output_labels[n] + "bytes")


def add(a, b):
    a = a + 1
    return a


def mult(a, b):
    a = a * b
    return a


def exp(a, b):
    a = a ** b
    return a


def subtract(x, y):
    return x - y


def convolve_small(image, kernel):
    if len(image.shape) == 2:
        image = da.asarray(convolve(image, kernel))
    elif len(image.shape) == 3:
        convolved = convolve(image[0,:,:], kernel)
        image[0, : ,:] = da.asarray(convolved)
    elif len(image.shape) == 4:
        convolved = convolve(image[:,:,0,0], kernel)
        image = da.asarray([convolved[0], convolved[1], 0, 0])
    else:
        raise ValueError("Cannot support more than 4D arrays")
    return image

def convolve_large(image, kernel):
    if len(image.shape) == 2:
        return da.asarray(convolve(image, kernel))        
    elif len(image.shape) == 3:
        convolved = convolve(image[0,:,:], kernel)
        image[0, :, :] = convolved   
        image = da.asarray(image)
    elif len(image.shape) == 4:
        convolved = convolve(image[:,:,0,0], kernel)
        return da.asarray([convolved[0], convolved[1], 0, 0])
    else:
        raise ValueError("Cannot support more than 4D arrays")
    return image

def s_kernel():
    return Gaussian2DKernel(x_stddev=1)


def l_kernel():
    return Gaussian2DKernel(x_stddev=5, y_stddev=5)

def get_divisors(n, i):
    div_list = []
    while i <= n/2:
        if n % i == 0:
            div_list.append(i)
        i = i + 1
    return div_list

def get_div_list(im_shape, ch_shape):
    div_list = []
    for i in range(len(im_shape)):
        div_list.append(get_divisors(im_shape[i], ch_shape[i]))
    return div_list
