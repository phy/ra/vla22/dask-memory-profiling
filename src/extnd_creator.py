import sys
sys.path.append(".")
import extnd
import numpy as np

def get_num_of_chunks(im_shape, chunk_shape):
    # currently, this will not cover the entire image if the dimensions of the chunk don't exactly divide the dimensions of the image.
    #TODO add checking /error handling for this
    # also assume that image and chunks have same number of dimensions. There may be a trivial fix to iterate over the number of dimensions of the chunk, rather than the image, and raise an error if chunk has more dims than the image.
    temp = []
    n_chunks = 1
    for i in range(len(im_shape)):
        x = im_shape[i] // chunk_shape[i]
        if x == 0:
            raise ValueError("Chunk dimension larger than image dimension in dimension %s", i)
        else:
            temp.append(x)
    for i in temp:
        n_chunks = n_chunks * i
    return n_chunks, temp

def get_chunk_location(chunk_size, n_chunks):
    loc = []
    n_ch = n_chunks[0]
    ch_dims = n_chunks[1]
    for i in range(n_ch):
        chunk_loc = []
        x = i % ch_dims[0]
        x = x * chunk_size[0]
        chunk_loc.append(x)
        if len(chunk_size) > 2:
            y = (i % (ch_dims[0] * ch_dims[1]))// ch_dims[0]
            y = y * chunk_size[1]
            chunk_loc.append(y)
            if len(chunk_size) > 3:
                z = i% (ch_dims[0] * ch_dims[1] * ch_dims[2]) // (ch_dims[0] * ch_dims[1])
                p = i // (ch_dims[0] * ch_dims[1] * ch_dims[2])
                z = z * chunk_size[2]
                p = p * chunk_size[3]
                chunk_loc.append(z)
                chunk_loc.append(p)
            else:
                z = i // (ch_dims[0] * ch_dims[1])
                z = z * chunk_size[2]
                chunk_loc.append(z)
        else:
            y = i // ch_dims[1]
            y = y * chunk_size[1]
            chunk_loc.append(y)
        loc.append(chunk_loc)
    return loc
def create_array(im, cs, logger=None, overlap=0, fill_value=None, kind="extnd"):
    ext_chunk_size = []
    for i in cs:
        i = i + overlap
        ext_chunk_size.append(i)
    chunk_array = [] 
    n_chunks = get_num_of_chunks(im.shape, cs)
    loc = get_chunk_location(cs, n_chunks)
    for i in loc:
        chunk = np.empty((ext_chunk_size))
        im_loc_list = []
        c_list = []
        for dim in range(len(cs)):
            x = i[dim] - overlap
            y = i[dim] + cs[dim] + overlap
            s = 0
            e = cs[dim]
            # code doesn't work with overlap > 0.
            if overlap > 0:
                if i[dim] == 0:
                    #overwrite the usual values
                    x = i[dim]
                    y = i[dim] + cs[dim] - overlap
                    s = s + overlap
                    chunk[dim][:overlap].fill(fill_value)
                elif i[dim] + cs[dim] + overlap >= im.shape[dim]:
                    y = im.shape[dim]
                    e = im.shape[dim]
                    chunk[dim][(loc[dim] + cs[dim])].fill(fill_value)
            c_list.append(slice(s, e))
            im_loc_list.append(slice(x, y))
        chunk[tuple(c_list)] = im[tuple(im_loc_list)]
        if kind == "extnd":
            chunk = extnd.enp(chunk, location=i, overlap=overlap, chunk_size=cs)
        chunk_array.append(chunk) 
    return chunk_array
if __name__ == "__main__":
    create_array(args)
