from dask.distributed import Client
import dask.array as da
import numpy as np

def thing():
    #with LocalCluster(threads_per_worker=2, n_workers=2, memory_limit="2GiB") as cluster, Client(cluster) as client:
    client = Client()
    a = da.arange(1000000, chunks=(1000))
    a = a +1
    a = a * 2
    a = a **3
    a.compute()
    b = np.array(a[:9])
    print(b)
    with open('/rds/user/vla22/hpc-work/output.txt', 'w') as file:
        file.write(np.array2string(b))
        
if __name__ == "__main__":
    thing()
