import logging
import n_d_loop 
import configparser
import argparse
from distributed import Client, LocalCluster

class ClusterDef():

    def __init__(self, config_read="../config/config.ini"):
        # parameters can be passed on the command line, to support slurm platforms where the parameters need to be fixed in the slurm script and then passed to the application, but the defaults are read from a config file. 
        #TODO make CLI args be written out to config file as a record
        config = configparser.ConfigParser()
        config.read(config_read)
        parser = argparse.ArgumentParser()
        parser.add_argument("--platform", help="specify the platform on which you are running this code", default=config['DEFAULT']['platform'])
        parser.add_argument("--dask-version", help="specify the version of dask used", default=config['DEFAULT']['dask_version'])
        parser.add_argument("--workflow", help="specify the workflow used", default=config['DEFAULT']['workflow'])
        parser.add_argument("--n_nodes", help="number of nodes", default=config['DEFAULT']['n_nodes'], type=int)
        parser.add_argument("--n_workers", help="number of workers", default=config['DEFAULT']['n_workers'], type=int)
        parser.add_argument("--worker_mem", help="RAM per worker", default=config['DEFAULT']['worker_mem'])
        parser.add_argument("--n_threads", help="threads per worker", default=config['DEFAULT']['n_threads'], type=int)
        parser.add_argument("--scheduler", help="dask scheduler")
        parser.add_argument("--image-config", help="tell us about the dataset to use, based on the config file", default="DEFAULT")
        args = parser.parse_args()
        platform = args.platform
        workflow = args.workflow
        dask_version = args.dask_version
        print(platform)
        n_nodes = args.n_nodes
        n_workers = args.n_workers
        print(args.worker_mem)
        worker_mem = args.worker_mem[:-3]
        print(worker_mem)
        n_threads = args.n_threads
        image_config = args.image_config
        log_level = config['DEFAULT']['log_level']
        logging.basicConfig(filename=config[platform]['output_dir']+"cluster.log", level=log_level)
        if config['DEFAULT']['csd3'] == "True":
            # This is for connecting to a client on a self-defined cluster, using a local variable to connect to the scheduler
            scheduler = args.scheduler
            client = Client(scheduler)
            logging.info("client connected")
            logging.info(client.scheduler_info())
            a = n_d_loop.ProfileMe(client=client, n_nodes=n_nodes, max_mem=config['DEFAULT'].getint('max_mem'), n_workers=n_workers, n_threads=n_threads, worker_mem=worker_mem, platform=platform, image_config=image_config, dask_version=dask_version, workflow=workflow)
            a.image_param_iter(function='fn')
        elif config['DEFAULT']['k8s'] == "True":
            # we use a bare client. If in a k8s environment, the config should connect to the scheduler automatically; otherwise it should fall back to a LocalCluster, suitable for local/single node testing.
            client = Client()
            a = n_d_loop.ProfileMe(client=client, worker_mem=worker_mem, max_mem=['DEFAULT'].getint('max_mem'), n_threads=n_threads, n_workers=n_workers, n_nodes=n_nodes, platform=platform, image_config=image_config, dask_version=dask_version, workflow=workflow)
            a.image_param_iter(function='fn')
        else:
            with LocalCluster(threads_per_worker=n_threads, n_workers=n_workers, memory_limit=(worker_mem + "GB")) as cluster, Client(cluster) as client:

            # we configure a local cluster
                a = n_d_loop.ProfileMe(client=client, worker_mem=worker_mem, max_mem=config['DEFAULT'].getint('max_mem'), n_threads=n_threads, n_workers=n_workers, n_nodes=n_nodes, platform=platform, image_config=image_config, dask_version=dask_version, workflow=workflow) 
                a.image_param_iter(function='fn')

if __name__ == "__main__":
    a = ClusterDef()
