from dask.distributed import Client 
import dask.array as da
import logging
import numpy as np
import os

scheduler = os.getenv("DASK_SCHEDULER")
client = Client(scheduler)
logging.basicConfig(filename="/rds/user/vla22/hpc-work/cluster.log", level=logging.DEBUG)
logging.info(client.scheduler_info())
logging.info("Dask prog started")
a = da.arange(100000000, chunks=(100000))
logging.info("Dask array assigned")
a = a +1
a = a * 2
a = a **3
a.compute()
b = np.array(a[:9])
logging.info(b)

