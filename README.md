# dask-memory-profiling
A project to profile Dask, and see how to set its operational parameters.

To run this project:

git clone it

create a virtual environment and activate it in the usual way

run `pip install -r requirements.txt`

Then change into the `dask-mem-tests` directory and run `python run_me.py`.

If desired, the `max_mem` parameter in `rune_me.py` can be increased.
