FROM python:3.9

COPY requirements.txt ./
COPY . . 
RUN pip install --upgrade pip
RUN pip install -I setuptools==58.4.0
RUN pip install -r requirements.txt

WORKDIR /dask-memory-profiling/src

EXPOSE 3000

CMD python run_me.py

