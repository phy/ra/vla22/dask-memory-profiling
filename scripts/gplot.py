import pandas as pd
import datetime
import matplotlib.pyplot as plt

#define the output directories to crawl over
outputdirs1 = ["/home/vla22/phd/dask-output/csd3-20230104/", "/home/vla22/phd/dask-output/4-coredesktop-dec2023/outputs/", "/home/vla22/phd/dask-output/loadtest-jan2023/outputs/"]
outputdirs2 = ["/home/vla22/phd/3d-aug-2023/", "/home/vla22/phd/3d-csd3-2023/", "/home/vla22/phd/dec2023/"]
outputdirs3 = [] #this will have platform already defined
outputdirs4 = ["/home/vla22/phd/outputs4/", '/home/vla22/phd/icelakeout/', '/home/vla22/phd/icelakejan24/', '/home/vla22/phd/cclakejan24/'] #this adds workflow and dask version
#define names of output files
prog_names = ["dask_array", "np_ext_fut_scatter", "np_ext_fut", "np_ext_delay", "np_array", "np_chunk", "np_chunk_delayed", "np_ext", "dask_fut", "dask_fut_scatter"]
prog_names2 = ["dask_array", "np_ext_fut_scatter", "np_ext_fut", "np_ext_delayed", "np_array", "np_chunk", "np_chunk_delayed", "np_ext", "dask_fut", "dask_fut_scatter"]

#define column names
names=["workers", "threads", "worker memory", "image size", "chunk size", "filetype", "file chunking", "duration", "load time", "processing time", "save time", "memory used", "memory scale"]  
names2=["nodes", "workers", "threads", "worker memory", "dimensions", "image size", "chunk size", "filetype", "file chunking", "duration", "load time", "processing time", "save time", "memory used", "memory scale"]
names3 = ["platform", "nodes", "workers", "threads", "worker memory", "dimensions", "image size", "chunk size", "filetype", "file chunking", "duration", "load time", "processing time", "save time", "memory used", "memory scale"]
names4 = ["platform", "dask_version", "workflow", "nodes", "workers", "threads", "worker memory", "dimensions", "image size", "chunk size", "filetype", "file chunking", "duration", "load time", "processing time", "save time", "memory used", "memory scale"]

str_format = " %H:%M:%S.%f"

def convert_time(time_string):
    # utility to confert time string to integer value of seconds
    temp_time = datetime.datetime.strptime(time_string, str_format)
    h = temp_time.hour * 3600
    s = temp_time.second
    m = temp_time.minute * 60
    f = temp_time.microsecond /1000000
    time_secs = h + s + m + f
    return time_secs

list_dfs = []
for i in outputdirs1:
    for j in prog_names:
        frame = pd.read_csv((i+j + ".csv"), names = names)
        frame['dur_secs'] = frame['duration'].apply(convert_time) 
        frame['load_s'] = frame['load time'].apply(convert_time)
        frame['proc_s'] = frame['processing time'].apply(convert_time)
        frame['save_s'] = frame['save time'].apply(convert_time)
        frame["nodes"] = 1
        frame["dimensions"] = 2
        frame["prog_name"] = j
        if 'csd3' in i:
            frame["platform"] = "csd3-skylake"
        elif 'desktop' in i:
            frame["platform"] = "4-core-desktop"
        elif 'loadtest' in i:
            frame["platform"] = "uis"
        frame["workflow"] = "add-mult-convolve"
        frame["dask_version"] = "2022.12.1"
        list_dfs.append(frame)

for i in outputdirs2:
    for j in prog_names:
        frame = pd.read_csv((i+j +".csv"), names = names2)
        frame['dur_secs'] = frame['duration'].apply(convert_time)
        frame['load_s'] = frame['load time'].apply(convert_time) 
        frame['proc_s'] = frame['processing time'].apply(convert_time)
        frame['save_s'] = frame['save time'].apply(convert_time)
        frame["prog_name"] = j
        frame["platform"] = " csd3-cclake"
        frame["workflow"] = "add-mult-convolve"
        frame["dask_version"] = "2022.12.1"
        list_dfs.append(frame)

for i in outputdirs3:
    for j in prog_names:
        frame = pd.read_csv((i+j +".csv"), names = names3)
        frame['dur_secs'] = frame['duration'].apply(convert_time)
        frame['load_s'] = frame['load time'].apply(convert_time) 
        frame['proc_s'] = frame['processing time'].apply(convert_time)
        frame['save_s'] = frame['save time'].apply(convert_time)
        frame["prog_name"] = j
        frame["workflow"] = "add-mult-convolve"
        frame["dask_version"] = "2022.12.1"
        list_dfs.append(frame)

for i in outputdirs4:
    for j in prog_names2:
        frame = pd.read_csv((i+j +".csv"), names = names4)
        frame['dur_secs'] = frame['duration'].apply(convert_time)
        frame['load_s'] = frame['load time'].apply(convert_time) 
        frame['proc_s'] = frame['processing time'].apply(convert_time)
        frame['save_s'] = frame['save time'].apply(convert_time)
        frame["prog_name"] = j
        list_dfs.append(frame)

md = pd.concat(list_dfs)
md["tot_workers"] = md["workers"] * md["nodes"]
md.to_csv("concat.csv")

# TODO - split out a small script that adds to concat.csv (to add new results), and then just read concat.csv in at the start now.


platform = md["platform"].value_counts() 
cs = md["chunk size"].value_counts()
ims = md["image size"].value_counts()
nodes = md["nodes"].value_counts()
threads = md["threads"].value_counts()
workers = md["workers"].value_counts()
worker_mem = md["worker memory"].value_counts()
print(platform)
print(cs)
print(ims)
print(nodes)
print(threads)
print(workers)
print(worker_mem)
sf1 = md[md["workflow"].str.contains("sf1")]
basic = md[md["workflow"].str.contains("add")]
csd3 = sf1[sf1["platform"].str.contains("csd3")]
cs = csd3["chunk size"].value_counts()
ims = csd3["image size"].value_counts()
nodes = csd3["nodes"].value_counts()
threads = csd3["threads"].value_counts()
workers = csd3["workers"].value_counts()
worker_mem = csd3["worker memory"].value_counts()
print(cs)
print(ims)
print(nodes)
print(threads)
print(workers)
print(worker_mem)
dup_rows = csd3[csd3.duplicated(["chunk size", "nodes", "threads", "workers", "worker memory", "filetype", "file chunking", "prog_name", "platform"])]
dup_rows.to_csv("params.csv")
dd = csd3[["prog_name", "platform", "nodes", "workers", "threads", "worker memory", "chunk size", "filetype", "file chunking", "dur_secs", "load_s", "proc_s", "save_s"]]
dd = dd.reset_index()
basic = basic.reset_index()

def label_rewriter(text):
    #takes a matplotlib text object, converts to a string, and rewrites for nicer labels.
    start_str = text.get_text()
    newt = start_str.replace("2894143", "medium")
    newt = newt.replace("1240347", "small")
    newt = newt.replace("8682429", "large")
    newt = newt.replace("csd3-","")
    return newt

w1 = csd3[csd3["workers"] == 1]
t6 = csd3[csd3["threads"] ==6]
t10 = csd3[csd3["threads"] ==10]
t12 = csd3[csd3["threads"]==12]
s_chunk = csd3[csd3["chunk size"] == 1240347]
s_chunk = s_chunk.reset_index()
fig = plt.figure(figsize=(8,6))
ax1  = fig.add_subplot()
#fig(figsize=(8,6))
fig2, ax2 = plt.subplots()
counts = [len(v) for k, v, in s_chunk.groupby(["threads", "prog_name"])]
s_chunk.plot.box(column="dur_secs", by=["threads", "prog_name"], ax=ax1)
s_chunk.plot.box(column="dur_secs", by=["worker memory", "prog_name"], ax=ax2)
ax1.set_xlabel("number of threads & program")
ax2.set_xlabel("worker RAM & program")
ax1.set_ylabel("duration in seconds")
ax2.set_ylabel("duration in seconds")
secax = ax1.secondary_xaxis('top')
for label in ax1.get_xticklabels(which='major'):
    label.set(rotation=30, fontsize='xx-small')
#this should be a list comprehension
clabels = [f'n= {x}' for x in counts ]
secax.set_xticks(ax1.get_xticks(), labels=clabels, rotation =45, fontsize='xx-small')
secax.set_xlabel("sample size")
for label in ax2.get_xticklabels(which='major'):
    label.set(rotation=30, fontsize='xx-small')
fig.savefig("smallchunks_threads")
fig2.savefig("small_ch_worker_mem")

bd = basic[basic["chunk size"] == 4096]
bd = bd.reset_index()
for i in ["csd3-skylake", "4-core-desktop", "uis", " csd3-cclake", " local-14core", " csd3-icelake", "csd3-cclake-himem"]:
    for j in ["proc_s", "dur_secs", "load_s", "save_s"]:
        fig = plt.figure()
        ax = fig.add_subplot()
        bd.plot.box(column=j, by=["prog_name", "chunk size"], ax=ax)
        ax.set_title("")
        ax.set_xlabel("program & chunk size")
        counts = [len(v) for k, v in bd.groupby(["prog_name", "chunk size"])]
        secax = ax.secondary_xaxis("top")
        secax.set_xlabel("sample size")
        clabels = [f'n= {x}' for x in counts]
        secax.set_xticks(ax.get_xticks(), labels=clabels, rotation=45, fontsize='xx-small')
        axlabels = []
        for t in ax.get_xticklabels():
            rewrit = label_rewriter(t)
            axlabels.append(rewrit)
        ax.set_xticks(ax.get_xticks(), labels=axlabels)
        for label in ax.get_xticklabels(which='major'):
            label.set(rotation=30, fontsize='xx-small')
        fig.savefig(str(i) +j)
        plt.close(fig)

samespec =  csd3[(csd3["workers"] == 2) & (csd3["threads"] ==8) & (csd3["worker memory"] == 125)]
samespec = samespec.reset_index()

fign = plt.figure()
axn = fign.add_subplot()
samespec.plot.box(column="load_s", by=["chunk size", "file chunking"], ax=axn)
axn.set_title("Data chunking effects on save and load times")
axn.set_xlabel("chunk size, and whether file was saved with chunks")
axn.set_ylabel("load time in seconds")
c = [len(v) for k, v in samespec.groupby(["chunk size", "file chunking"])]
sax = axn.secondary_xaxis("top")
sax.set_xlabel("sample size")
clab = [f'n = {x}' for x in c]
sax.set_xticks(axn.get_xticks(), labels=clab, rotation=45, fontsize='xx-small')
axnlabels = []
for t in axn.get_xticklabels():
    rewrit = label_rewriter(t)
    axnlabels.append(rewrit)
axn.set_xticks(axn.get_xticks(), labels=axnlabels)
for label in axn.get_xticklabels(which='major'):
    label.set(rotation=30, fontsize='xx-small')
fign.savefig("loadtf")
plt.close(fign)
fign = plt.figure()
axn = fign.add_subplot()
samespec.plot.box(column="save_s", by=["chunk size", "file chunking"], ax=axn)
axn.set_title("Data chunking effects on save and load times")
axn.set_xlabel("chunk size, and whether file was saved with chunks")
axn.set_ylabel("save time in seconds")
c = [len(v) for k, v in samespec.groupby(["chunk size", "file chunking"])]
sax = axn.secondary_xaxis("top")
sax.set_xlabel("sample size")
clab = [f'n = {x}' for x in c]
sax.set_xticks(axn.get_xticks(), labels=clab, rotation=45, fontsize='xx-small')
axnlabels = []
for t in axn.get_xticklabels():    
    rewrit = label_rewriter(t)    
    axnlabels.append(rewrit)    
axn.set_xticks(axn.get_xticks(), labels=axnlabels)    
for label in axn.get_xticklabels(which='major'):    
    label.set(rotation=30, fontsize='xx-small')
fign.savefig("savetf")
plt.close(fign)
csl = [1240347, 2894143, 8682429]
for i in csl:
    d2 = samespec[samespec["chunk size"] == i]
    d2 = d2.reset_index()
    for j in ["proc_s", "dur_secs", "load_s", "save_s"]:
        if not d2.empty:
            fig = plt.figure(figsize=(8,6))
            ax = fig.add_subplot()
            d2.plot.box(column=j, by=["prog_name", "platform"], ax=ax)
            ax.set_title(str(i) + j)
            ax.set_xlabel("program & platform")
            ax.set_ylabel(j + "in seconds")
            counts = [len(v) for k, v in d2.groupby(["prog_name", "platform"])]
            secax = ax.secondary_xaxis("top")
            secax.set_xlabel("sample size")
            clabels = [f'n= {x}' for x in counts]
            secax.set_xticks(ax.get_xticks(), labels=clabels, rotation=45, fontsize='xx-small')
            for label in ax.get_xticklabels(which='major'):
                label.set(rotation=30, fontsize='xx-small')
            fig.savefig(str(i) +j)
            plt.close(fig)
wt_list = [w1, t6, t10, t12]
j = 0

fig3 = plt.figure(figsize=(8,6))
ax3 = fig3.add_subplot()
csd3 = csd3.reset_index()
csd3.plot.box(column="dur_secs", by=["tot_workers", "threads"], ax=ax3)
ax3.set_title("")
ax3.set_xlabel("Total number of workers  and threads per worker")
ax3.set_ylabel("duration in seconds")
counts = [len(v) for k, v in csd3.groupby(["tot_workers", "threads"])]
secax = ax3.secondary_xaxis("top")
secax.set_xlabel("sample size")
clabels = [f'n= {x}' for x in counts]
secax.set_xticks(ax3.get_xticks(), labels=clabels, rotation=45, fontsize='xx-small')
for label in ax3.get_xticklabels(which='major'):
    label.set(rotation=30, fontsize='xx-small')
fig3.savefig("nt")
plt.close(fig3)
for i in wt_list:
    i = i.reset_index()
    if j == 0:
        k = "one worker"
    elif j == 1:
        k = "6 threads"
    elif j == 2:
        k = "10 threads"
    else:
        k = "12 threads"
    j = j +1
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, layout="constrained")
    i.plot.box(column="load_s", by=["chunk size", "nodes"], ax=ax1)
    counts1 = [len(v) for k, v in i.groupby(["chunk size", "nodes"])]
    secax1 = ax1.secondary_xaxis("top")
    secax1.set_xlabel("sample size")
    clabels1 = [f'n= {x}' for x in counts1]
    secax1.set_xticks(ax1.get_xticks(), labels=clabels1, rotation=45, fontsize='xx-small')
    ax1labels = []
    for t in ax1.get_xticklabels():
        rewrit = label_rewriter(t)
        ax1labels.append(rewrit)
    ax1.set_xticks(ax1.get_xticks(), labels=ax1labels)

    ax1.set_title("")
    ax1.set_ylabel("load time in seconds")
    i.plot.box(column="save_s", by=["chunk size", "nodes"], ax=ax2)
    counts2 = [len(v) for k, v in i.groupby(["chunk size", "nodes"])]
    secax2 = ax2.secondary_xaxis("top")
    secax2.set_xlabel("sample size")
    clabels2 = [f'n= {x}' for x in counts2]
    secax2.set_xticks(ax2.get_xticks(), labels=clabels2, rotation=45, fontsize='xx-small')
    ax2labels = []
    for t in ax2.get_xticklabels():
        rewrit = label_rewriter(t)
        ax2labels.append(rewrit)
    ax2.set_xticks(ax2.get_xticks(), labels=ax2labels)
    ax2.set_ylabel("save time in seconds")
    ax2.set_title("")
    i.plot.box(column="proc_s", by=["chunk size", "nodes"], ax=ax3)
    counts3 = [len(v) for k, v in i.groupby(["chunk size", "nodes"])]
    secax3 = ax3.secondary_xaxis("top")
    secax2.set_xlabel("sample size")
    clabels3 = [f'n= {x}' for x in counts3]
    secax3.set_xticks(ax3.get_xticks(), labels=clabels2, rotation=45, fontsize='xx-small')
    ax3labels = []
    for t in ax3.get_xticklabels():
        rewrit = label_rewriter(t)
        ax3labels.append(rewrit)
    ax3.set_xticks(ax3.get_xticks(), labels=ax3labels)
    ax3.set_title("")
    secax3.set_xlabel("sample size")
    ax3.set_ylabel("processing time in seconds")
    i.plot.box(column="dur_secs", by=["chunk size", "nodes"], ax=ax4)
    counts4 = [len(v) for k, v in i.groupby(["chunk size", "nodes"])]
    secax4 = ax4.secondary_xaxis("top")
    secax4.set_xlabel("sample size")
    ax4.set_title("")
    clabels4 = [f'n= {x}' for x in counts4]
    secax4.set_xticks(ax4.get_xticks(), labels=clabels2, rotation=45, fontsize='xx-small')
    ax4labels = []
    for t in ax4.get_xticklabels():
        rewrit = label_rewriter(t)
        ax4labels.append(rewrit)
    ax4.set_xticks(ax4.get_xticks(), labels=ax4labels)
    fig.suptitle(f"{k}")
    fig.supxlabel("chunk size in voxels")
    ax4.set_ylabel("total duration in seconds")
    for label in ax1.get_xticklabels(which='major'):
        label.set(rotation=90, fontsize='xx-small')

    for label in ax2.get_xticklabels(which='major'):
        label.set(rotation=90, fontsize='xx-small')
    for label in ax3.get_xticklabels(which='major'):
        label.set(rotation=90, fontsize='xx-small')
    for label in ax4.get_xticklabels(which='major'):
        label.set(rotation=90, fontsize='xx-small')
    fig.savefig(k)

im_size_list = [4096, 8192, 16384, 34729716, 268435456]
for i in im_size_list:
    d = basic[basic["image size"] ==i]
    data = d.reset_index()
    for j in prog_names:
        d3 = data[data["prog_name"].str.contains(j)]
        for k in ["csd3-skylake", "4-core-desktop", "uis", "csd3-cclake", " local-14core", "csd3-icelake", "csd3-cclake-himem"]:
        #data2 = data2.reset_index()
            data2 = d3[d3["platform"].str.contains(k)]
            if not data2.empty:
                fig = plt.figure(figsize=(8,6), layout="constrained")
                fig2 = plt.figure(figsize=(8,6), layout="constrained")
                f1axes = []
                f2axes = []
                ax1 = fig.add_subplot() 
                ax2 = fig2.add_subplot() 
                data2.plot.box(column="load_s", by=["chunk size", "filetype"], ax=ax1)
                data2.plot.box(column="save_s", by=["chunk size", "filetype"], ax=ax2)
                ax1.set_ylabel("time in seconds")
                ax1.set_title(f"Load time for {i} pixel image with basic workflow for the {j} program on the {k} cluster", fontsize="medium")
                ax2.set_title(f"Save time for {i} pixel image with basic workflow for the {j} program on the {k} cluster", fontsize="medium")
                for label in ax1.get_xticklabels(which="major"):
                    label.set(rotation=30, fontsize='xx-small')
                for label in ax2.get_xticklabels(which="major"):
                    label.set(rotation=30, fontsize='xx-small')
                counts = [len(v) for k, v in data2.groupby(["chunk size", "filetype"])]
                secax = ax1.secondary_xaxis("top")
                for label in ax1.get_xticklabels(which='major'):
                    label.set(rotation=30, fontsize='xx-small')
                clabels = [f'n= {x}' for x in counts]
                secax.set_xticks(ax1.get_xticks(), labels=clabels, rotation=45, fontsize='xx-small')
                secax2 = ax2.secondary_xaxis("top")
                for label in ax2.get_xticklabels(which='major'):
                    label.set(rotation=30, fontsize='xx-small')
                clabels2 = [f'n= {x}' for x in counts]
                secax2.set_xticks(ax2.get_xticks(), labels=clabels2, rotation=45, fontsize='xx-small')
                fig.savefig("load" + str(i)+ j +k)
                fig2.savefig("save" + str(i)+ j+ k)
                plt.close(fig)
                plt.close(fig2)

csd32 = csd3[csd3["nodes"] ==2]
csd33 = csd3[csd3["nodes"] == 3]
csd34 = csd3[csd3["nodes"] ==4]
skylake = md[md["platform"].str.contains("skylake")]
cclake = md[md["platform"].str.contains("cclake")]
icelake = md[md["platform"].str.contains("icelake")]
uis = md[md["platform"].str.contains("uis")]
local4 = md[md["platform"].str.contains("4-core-desktop")]
local14 = md[md["platform"].str.contains("local-14")]

