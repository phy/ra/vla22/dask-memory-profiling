#!/bin/bash
#!
#! Example SLURM job script for Peta4-CascadeLake (Cascade Lake CPUs, HDR IB)
#! Last updated: Fri 18 Sep 12:24:48 BST 2020
#!

#!#############################################################
#!#### Modify the options in this section as appropriate ######
#!#############################################################

#! sbatch directives begin here ###############################
#! Name of the job:
#SBATCH -J cpujob
#! Which project should be charged:
#SBATCH -A SKA-SDP-SL3-CPU 
#SBATCH -p cclake
#! How many whole nodes should be allocated?
#SBATCH --nodes=2
#! How many (MPI) tasks will there be in total? (<= nodes*56)
#! The Cascade Lake (cclake) nodes have 56 CPUs (cores) each and
#! 3420 MiB of memory per CPU.
#SBATCH --mem=95760
#SBATCH --ntasks=4
#! How much wallclock time will be required?
#SBATCH --time=06:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=NONE
#! Uncomment this to prevent the job from being requeued (e.g. if
#! interrupted by node failure or system downtime):
##SBATCH --no-requeue

#! sbatch directives end here (put any additional directives above this line)

#! Notes:
#! Charging is determined by cpu number*walltime.
#! The --ntasks value refers to the number of tasks to be launched by SLURM only. This
#! usually equates to the number of MPI tasks launched. Reduce this from nodes*56 if
#! demanded by memory requirements, or if OMP_NUM_THREADS>1.
#! Each task is allocated 1 CPU by default, and each CPU is allocated 3420 MiB
#! of memory. If this is insufficient, also specify
#! --cpus-per-task and/or --mem (the latter specifies MiB per node).

#! Number of nodes and tasks per node allocated by SLURM (do not change):
numnodes=$SLURM_JOB_NUM_NODES
numtasks=$SLURM_NTASKS
mpi_tasks_per_node=$(echo "$SLURM_TASKS_PER_NODE" | sed -e  's/^\([0-9][0-9]*\).*$/\1/')
#! ############################################################
#! Modify the settings below to specify the application's environment, location 
#! and launch method:

#! Optionally modify the environment seen by the application
#! (note that SLURM reproduces the environment at submission irrespective of ~/.bashrc):
. /etc/profile.d/modules.sh                # Leave this line (enables the module command)
module purge                               # Removes all modules still loaded
module load rhel7/default-ccl              # REQUIRED - loads the basic environment

#! Insert additional module load commands after this line if needed:
module load hdf5/1.12.1
#! safe value to no more than 56:
export OMP_NUM_THREADS=1

#! Number of MPI tasks to be started by the application per node and in total (do not change):
np=$[${numnodes}*${mpi_tasks_per_node}]

#! The following variables define a sensible pinning strategy for Intel MPI tasks -
#! this should be suitable for both pure MPI and hybrid MPI/OpenMP jobs:
export I_MPI_PIN_DOMAIN=omp:compact # Domains are $OMP_NUM_THREADS cores in size
export I_MPI_PIN_ORDER=scatter # Adjacent domains have minimal sharing of caches/sockets
#! Notes:
#! 1. These variables influence Intel MPI only.
#! 2. Domains are non-overlapping sets of cores which map 1-1 to MPI tasks.
#! 3. I_MPI_PIN_PROCESSOR_LIST is ignored if I_MPI_PIN_DOMAIN is set.
#! 4. If MPI tasks perform better when sharing caches/sockets, try I_MPI_PIN_ORDER=compact.

cd $SLURM_SUBMIT_DIR
echo -e "Changed directory to `pwd`.\n"

JOBID=$SLURM_JOB_ID
# this is vital to ensure the python environment is correctly set up
source ../../vevn/bin/activate
echo ${SLURM_JOB_NODELIST}
nworkers=$((numtasks/numnodes))
#the worker_mem should be adjusted according to the available RAM/worker
worker_mem=10GiB
#set to the desired max number of threads
nthreads=20
#! Create a hostfile:
scontrol show hostnames $SLURM_JOB_NODELIST | uniq > hostfile.$JOBID
# set the scheduler variable based on the first node in the hostfile
scheduler=$(head -1 hostfile.$JOBID)
echo -e "$scheduler"
sleep 40
# iterate over the hostfile to launch scheduler on the first node, and workers on all nodes, included the scheduler node.
hostIndex=0
for host in `cat hostfile.$JOBID`; do
    echo "Working on $host ...."
# for all hosts, the $PYTHONPATH of the venv set up earlier must be exported in order to propagate the environment to all nodes.
    if [ "$hostIndex" = "0" ]; then
        ssh $host "export PATH=$PATH PYTHONPATH=$PYTHONPATH; which python; dask scheduler --port=8786" &
          sleep 10
        
         echo "run dask scheduler"
    else
        ssh $host "export PATH=$PATH PYTHONPATH=$PYTHONPATH; dask worker --host=${host} --nworkers=$nworkers --nthreads=$nthreads --memory-limit=$worker_mem $scheduler:8786" &
        sleep 1
        echo "installing worker"
    fi
    hostIndex="1"
done
echo "Scheduler and workers now running"

CMD="DASK_SCHEDULER=${scheduler}:8786 NNODES=${numnodes} NWORKERS=${nworkers} NTHREADS=${nthreads} WORKER_MEM=${worker_mem} python ../src/run_image_proc.py"
echo "About to execute $CMD"
eval $CMD 
