import sys
sys.path.insert(0, "../src/")
import helpers
import dask.array as da

#sys.argv1 is the path to the file, 2 is the array size you want to load, 3 is the chunk size you want to work with.
def convert(filename, arraysize, chunk_size):
    arraysize = arraysize.split(",")
    chunk_size = chunk_size.split(",")
    inputarr = da.from_array(helpers.load_file('fits', filename, arraysize, 'dask'))
    inputc = da.from_array(helpers.load_file('fits', filename, arraysize, 'dask', chunk_size))
    fpath = filename[:-5]
    #helpers.save_file(fpath, 'hdf5', inputarr)
    #helpers.save_file(fpath, "zarr", inputarr)
    #helpers.save_file
    helpers.save_file(fpath+"_chunked", "zarr", inputc, chunks=chunk_size)


if __name__ == "__main__":
    convert(sys.argv[1], sys.argv[2], sys.argv[3])

