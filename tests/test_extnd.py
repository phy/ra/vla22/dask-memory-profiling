import sys
sys.path.insert(0, "../src/")
import extnd_creator as wrap
import extnd
import numpy as np

def test_get_chunk_loc_2D():
    a = wrap.get_chunk_location([3,3], (9, [3,3]))
    assert a == [[0, 0], [3, 0], [6, 0], [0, 3], [3, 3], [6, 3], [0, 6], [3, 6], [6, 6]]

def test_get_chunk_loc_3D():
    a = wrap.get_chunk_location([3,3,3], (27, [3,3,3]))
    assert a == [[0, 0, 0], [3, 0, 0], [6, 0, 0], [0, 3, 0], [3, 3, 0], [6, 3, 0], [0, 6, 0], [3, 6, 0], [6, 6, 0], [0, 0, 3], [3, 0, 3], [6, 0, 3], [0, 3, 3], [3, 3, 3], [6, 3, 3], [0, 6, 3], [3, 6, 3], [6, 6, 3], [0, 0, 6], [3, 0, 6], [6, 0, 6], [0, 3, 6], [3, 3, 6], [6, 3, 6], [0, 6, 6], [3, 6, 6], [6, 6, 6]]

def test_get_chunk_loc_4D():
    a = wrap.get_chunk_location([3,3,3,4], (108, [3,3,3,4]))
    assert a == [[0, 0, 0, 0], [3, 0, 0, 0], [6, 0, 0, 0], [0, 3, 0, 0], [3, 3, 0, 0], [6, 3, 0, 0], [0, 6, 0, 0], [3, 6, 0, 0], [6, 6, 0, 0], [0, 0, 3, 0], [3, 0, 3, 0], [6, 0, 3, 0], [0, 3, 3, 0], [3, 3, 3, 0], [6, 3, 3, 0], [0, 6, 3, 0], [3, 6, 3, 0], [6, 6, 3, 0], [0, 0, 6, 0], [3, 0, 6, 0], [6, 0, 6, 0], [0, 3, 6, 0], [3, 3, 6, 0], [6, 3, 6, 0], [0, 6, 6, 0], [3, 6, 6, 0], [6, 6, 6, 0], [0, 0, 0, 4], [3, 0, 0, 4], [6, 0, 0, 4], [0, 3, 0, 4], [3, 3, 0, 4], [6, 3, 0, 4], [0, 6, 0, 4], [3, 6, 0, 4], [6, 6, 0, 4], [0, 0, 3, 4], [3, 0, 3, 4], [6, 0, 3, 4], [0, 3, 3, 4], [3, 3, 3, 4], [6, 3, 3, 4], [0, 6, 3, 4], [3, 6, 3, 4], [6, 6, 3, 4], [0, 0, 6, 4], [3, 0, 6, 4], [6, 0, 6, 4], [0, 3, 6, 4], [3, 3, 6, 4], [6, 3, 6, 4], [0, 6, 6, 4], [3, 6, 6, 4], [6, 6, 6, 4], [0, 0, 0, 8], [3, 0, 0, 8], [6, 0, 0, 8], [0, 3, 0, 8], [3, 3, 0, 8], [6, 3, 0, 8], [0, 6, 0, 8], [3, 6, 0, 8], [6, 6, 0, 8], [0, 0, 3, 8], [3, 0, 3, 8], [6, 0, 3, 8], [0, 3, 3, 8], [3, 3, 3, 8], [6, 3, 3, 8], [0, 6, 3, 8], [3, 6, 3, 8], [6, 6, 3, 8], [0, 0, 6, 8], [3, 0, 6, 8], [6, 0, 6, 8], [0, 3, 6, 8], [3, 3, 6, 8], [6, 3, 6, 8], [0, 6, 6, 8], [3, 6, 6, 8], [6, 6, 6, 8], [0, 0, 0, 12], [3, 0, 0, 12], [6, 0, 0, 12], [0, 3, 0, 12], [3, 3, 0, 12], [6, 3, 0, 12], [0, 6, 0, 12], [3, 6, 0, 12], [6, 6, 0, 12], [0, 0, 3, 12], [3, 0, 3, 12], [6, 0, 3, 12], [0, 3, 3, 12], [3, 3, 3, 12], [6, 3, 3, 12], [0, 6, 3, 12], [3, 6, 3, 12], [6, 6, 3, 12], [0, 0, 6, 12], [3, 0, 6, 12], [6, 0, 6, 12], [0, 3, 6, 12], [3, 3, 6, 12], [6, 3, 6, 12], [0, 6, 6, 12], [3, 6, 6, 12], [6, 6, 6, 12]]

def test_get_chunk_loc_3D2():
    #test that with one dimension being 1 pixel wide, it works
    a = wrap.get_chunk_location((3,3,1), (9, [3,3,9]))
    assert a == [[0, 0, 0], [3, 0, 0], [6, 0, 0], [0, 3, 0], [3, 3, 0], [6, 3, 0], [0, 6, 0], [3, 6, 0], [6, 6, 0]]

def test_get_chunk_sdc2_size():
    a = wrap.get_chunk_location((7,643,643), (12, [3,2,2]))
    assert a == [[0, 0, 0], [7, 0, 0], [14, 0, 0], [0, 643, 0], [7, 643, 0], [14, 643, 0], [0, 0, 643], [7, 0, 643], [14, 0, 643], [0, 643, 643], [7, 643, 643], [14, 643, 643]]

def test_get_num_chunks_2D():
    a = wrap.get_num_of_chunks((9,9), [3,3])
    assert a == (9, [3,3])

def test_get_num_chunks_3D():
    a = wrap.get_num_of_chunks((9, 9, 9), [3, 3, 3])
    assert a == (27, [3,3,3])

def test_get_num_chunks_4D():
    a = wrap.get_num_of_chunks((4, 4, 4, 4), [2, 2, 2, 2])
    assert a == (16, [2,2,2,2])

def test_get_num_chunks_Dis1():
    a = wrap.get_num_of_chunks((9, 9, 9), [3, 3, 1])
    assert a == (81, [3,3,9])

#def test_get_num_chunks_cs_too_large():
    #//TODO

#def test_get_num_chunks_cs_lower_dim():
    #//TODO

#def test_get_num_chunks_cs_more_dims():
    #//TODO

def test_create_2Darray():
    arr = np.arange(16)
    array = np.reshape(arr,(4,4))
    a = wrap.create_array(array, (2,2))
    assert len(a) == 4
    assert isinstance(a[0], extnd.enp)
    assert a[0].shape == (2,2)
    assert a[0][0][0] == 0.0
    assert a[3][1][1] == 15

def test_create_3Darray():
    arr = np.arange(64)
    array = np.reshape(arr, (4,4,4))
    a = wrap.create_array(array, (2,2,2))
    assert len(a) == 8
    assert a[5].shape == (2,2,2)
    assert a[7][1][1][1] == 63

def test_create_4Darray():
    arr = np.arange(256)
    array = np.reshape(arr, (4,4,4,4))
    a = wrap.create_array(array, (2,2,2,2))
    assert len(a) == 16
    assert a[10].shape == (2,2,2,2)
    assert a[15][1][1][1][1] == 255

