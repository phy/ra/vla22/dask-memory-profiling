import sys
sys.path.insert(0, "../src/")
#import run_image_proc as iproc
import configparser
from dask.distributed import LocalCluster, Client
import numpy as np
import n_d_loop as ndl

#def test_set_image_params():
 #   config = configparser.ConfigParser()
  #  config.read('../config/config.ini')
   # a = eg_loop.ProfileMe(client=None, worker_mem=config['workers'].getint('worker_mem'), threads=config['workers'].getint('n_threads'), workers=config['workers'].getint('n_workers'), n_nodes=config['cluster'].getint('n_nodes'))
    #b = a.set_im_params()
    #assert b == (512, 512)


#def test_set_chunk_params():
 #   config = configparser.ConfigParser()
  #  config.read('../config/config.ini')
   # a = eg_loop.ProfileMe(client=None, worker_mem=config['workers'].getint('worker_mem'), threads=config['workers'].getint('n_threads'), workers=config['workers'].getint('n_workers'), n_nodes=config['cluster'].getint('n_nodes'))
    #b = a.set_chunk_params()
    #assert b == [128, 128]

#def test_eg_loop():
 #   config = configparser.ConfigParser()
  #  config.read('../config/config.ini')
   # cluster = LocalCluster(threads_per_worker=config['workers'].getint('n_threads'), n_workers=config['workers'].getint('n_workers'), memory_limit=config['workers']['worker_mem'] + "GB")
    #client = Client(cluster)
    #a = eg_loop.ProfileMe(client=client, worker_mem=config['workers'].getint('worker_mem'), threads=config['workers'].getint('n_threads'), workers=config['workers'].getint('n_workers'), n_nodes=config['cluster'].getint('n_nodes'))
    #a.image_param_iter(function='fn')
    #assert np.all(a.verify) == np.all([[1.79740815, 2.07637973, 2.34365892, 2.58985693, 2.80789273], [2.07637985, 2.39862315, 2.70735431, 2.99172907, 3.24356944], [2.3436592,  2.70735447, 3.05579296, 3.37673636, 3.66095616], [2.58985737, 2.9917294,  3.37673654, 3.73135675, 4.04539432], [2.80789334, 3.24356994, 3.66095652, 4.0453945,  4.38583197]])
def test_nd_loop():
    config = configparser.ConfigParser()
    config.read('../config/config.ini')
    cluster = LocalCluster(threads_per_worker=config['DEFAULT'].getint('n_threads'), n_workers=config['DEFAULT'].getint('n_workers'), memory_limit=config['DEFAULT']['worker_mem'])
    client = Client(cluster)
    temp = config['DEFAULT']['worker_mem']
    print(type(temp))
    temp = temp[:-2]
    a = ndl.ProfileMe(client=client, n_nodes=config['DEFAULT'].getint('n_nodes'), max_mem=config['DEFAULT'].getint('max_mem'), n_workers=config['DEFAULT'].getint('n_workers'), n_threads=config['DEFAULT'].getint('n_threads'), worker_mem=temp, platform=config['DEFAULT']['platform'], image_config='DEFAULT', dask_version=config['DEFAULT']['dask_version'], workflow=config['DEFAULT']['workflow'])
    a.image_param_iter(function='fn')
    assert np.all(a.verify) == np.all([[1.79740815, 2.07637973, 2.34365892, 2.58985693, 2.80789273], [2.07637985, 2.39862315, 2.70735431, 2.99172907, 3.24356944], [2.3436592,  2.70735447, 3.05579296, 3.37673636, 3.66095616], [2.58985737, 2.9917294,  3.37673654, 3.73135675, 4.04539432], [2.80789334, 3.24356994, 3.66095652, 4.0453945,  4.38583197]])

#def test_wrapper():

#    a = iproc.instantiate_cluster(config_read="config/test_config.ini")
#    assert a.client.ncores
