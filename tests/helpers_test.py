import sys
sys.path.insert(0, "../src/")
import helpers
import numpy as np
import pytest
import os.path
import dask.array as da

@pytest.fixture
def array():
    """ Produces a 4x4 numpy array, which can be used in other tests"""
    a = np.arange(16)
    b = np.reshape(a, (4, 4))
    return b

@pytest.fixture
def d_array():
    """ Calls the array() function to take a 4x4 numpy array and turns it into an equivalent Dask array with 2x2 chunks
    """
    temp = np.reshape(np.arange(16), (4,4))
    a = da.from_array(temp, chunks=(2,2))
    return a

@pytest.fixture
def d_array_3d():
    temp = np.reshape(np.arange(64), (4,4,4))
    a = da.from_array(temp, chunks=(2,2,2))
    return a

def test_save_fits(tmpdir, array):
    """
    Takes an array, and saves it as a fits file.
    Tests that the file exists.
    """
    path = tmpdir.join('sfits')
    helpers.save_file(path, "fits", array, is_np=True)
    assert os.path.exists(tmpdir.join('sfits.fits')) == True

def test_save_zarr(tmpdir, d_array):
    """
    Takes an array, and saves it as a zarr file.
    Tests that the zarr file exists.
    """
    path = tmpdir.join('szarr')
    helpers.save_file(path, "zarr", d_array)
    assert os.path.exists(tmpdir.join("szarr.zarr")) == True

def test_save_hdf5(tmpdir, d_array):
    """
    Takes an array, and saves it as an hdf5 file.
    Tests that the hdf5 file exists.
    """
    path = tmpdir.join('shdf5')
    helpers.save_file(path, "hdf5", d_array)
    assert os.path.exists(tmpdir.join("shdf5.hdf5")) == True

def test_load_fits():
    """
    Tests that loading a fits file works, by asserting that a known value is in the array at the correct position.
    """
    a = helpers.load_file("fits", "../data/SKAMid_B5_8h_v3.fits", (16, 16), 'np')
    print(a.shape)
    assert a[1, 1] == pytest.approx(1.0220805e-06)

def test_load_zarr():
    """
    Tests that loading a zarr file works, by asserting that a known value in the array at the correct position.
    """
    a = helpers.load_file("zarr", "../data/SKAMid_B5_8h_v3.zarr", (16, 16), 'dask', chunk_shape=(4,4))
    b = helpers.load_file("zarr", "../data/SKAMid_B5_8h_v3.zarr", (16, 16), 'np')
    assert np.array(a[1,1]) == pytest.approx(1.0220805e-06)
    assert b[1,1] == pytest.approx(1.0220805e-06)

def test_load_hdf5():
    """
    Tests that loading an hdf5 file works, by asserting that a known value is in the array at the correct position.
    """
    a = helpers.load_file("hdf5", "../data/SKAMid_B5_8h_v3_hdf5.h5", (16, 16), 'dask', chunk_shape=(4,4))
    b = helpers.load_file("hdf5", "../data/SKAMid_B5_8h_v3_hdf5.h5", (16, 16), 'np')
    assert np.array(a[1,1]) == pytest.approx(1.0220805e-06)
    assert b[1,1] == pytest.approx(1.0220805e-06)
